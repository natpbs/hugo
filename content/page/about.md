---
title: About Nathan
subtitle: ""
comments: false
---

## Short Bio
Nathan Seddig is a young mathematical engineer and computer science enthusiast. His passions include cybersecurity, computer science and all innovative technologies.

Freshly graduated at Polytechnic University of Turin, he is looking forward to new opportunities at digital innovation driven companies.
